# Changelog
All notable changes to this project will be documented in this file.

## [1.1.0] - 2021-02-10

### Added
- TypoScript and Fluid partials for the TYPO3 Bootstrap Package
- Added CHANGELOG file

### Updated
- Updated Font Awesome Library from "5.15.1-web" to "5.15.2-web"!
- README file: https://bitbucket.org/typo3freelancer/faicon/src/master/README.md

## [1.0.0] - 2021-02-01

### Hello World!
- Initial release after some testing
